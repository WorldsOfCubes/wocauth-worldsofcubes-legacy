<?php
require('../system.php');

function logExit($text, $output = "1") {
  vtxtlog($text); exit($output);
}

if (empty($_POST['user']) or empty($_POST['password'])) 

	logExit("[api/auth.php] login process [Empty input] [ ".((empty($_POST['user']))? 'LOGIN ':'').((empty($_POST['password']))? 'PASSWORD ':'')."]");

	require(MCR_ROOT.'instruments/user.class.php'); 
	BDConnect('auth');

	$login = $_POST['user']; $password = $_POST['password'];

if (!preg_match("/^[a-zA-Z0-9_-]+$/", $password)) 
		
	logExit("[api/auth.php] login process [Bad symbols] User [$login] Password [*sensored*]");		
    

	
	$auth_user = new User($login, $bd_users['login']);
	
	if ( !$auth_user->id() ){
		$auth_user = new User($login, $bd_users['email']);
		
		if ( !$auth_user->id() ) logExit("[api/auth.php] login process [Unknown user] User [$login] Password [*sensored*]", "2");
	}
	if ( $auth_user->lvl() <= 1 ) exit("3");
	if ( !$auth_user->authenticate($password) ) logExit("[api/auth.php] login process [Wrong password] User [$login] Password [*sensored*]");

	vtxtlog("[api/auth.php] login process [Success] User [$login]");			
		
	exit('WoCapi:'.$auth_user->gender().':'.$auth_user->name().':'.$auth_user->email().':');
?>